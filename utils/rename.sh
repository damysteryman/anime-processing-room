#!/bin/bash

is_integer() {
	if [ -n "$1" ] && [ "$1" -eq "$1" ] 2>/dev/null; then
		echo 1
	else
		echo 0
	fi
}

dir=`pwd`
season=1

# Argument parser
# based on https://dzone.com/articles/bash-snippet-parsing-command-line-arguments-and-as
while [[ $# -gt 0 ]]
do
    key="${1}"

    case ${key} in
    -h|--help)
        echo "No help"
        shift # past argument
        ;;
	-s|--season)
		if [ $(is_integer ${2}) -eq 1 ]; then
			season=${2}
			shift # past argument
			shift # past value
		else
			echo "ERROR: Specified Season value (${2}) is invalid."
			exit 1
		fi
		;;
	-d|--dir)
		if [ -d "${2}" ]; then
			dir="${2}"
			shift # past argument
			shift # past value
		else
			echo "ERROR: Specified Directory (${2}) is invalid."
			exit 1
		fi
		;;
    *)    # unknown option
        shift # past argument
        ;;
    esac
#     shift
done

seasonStr=""
if [ $season -lt 9 ]; then
	seasonStr="0$season"
else
	seasonStr="$season"
fi

count=1
for f in $(ls -q "${dir}" | grep mkv | grep -v '^S[0-9][0-9]E' | sort)
do
	epNo=""
	if [ $count -le 9 ]; then
		epNo="0${count}"
	else
		epNo="${count}"
	fi
	
	echo "$f -> ${dir}/S${seasonStr}E${epNo}.mkv"
	mv "${dir}/$f" "${dir}/S${seasonStr}E${epNo}.mkv"
	((count++))
done