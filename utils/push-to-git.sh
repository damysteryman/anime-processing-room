rev=$(( $(git rev-list --count HEAD) + 1 ))
echo "UPDATING TO REV: $rev"
sed -i s/rev=.*/rev=${rev}/g anime-processing-room
git add *
git commit
git push origin master