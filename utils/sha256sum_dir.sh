#!/bin/bash

if [ -d "$1" ]; then
	start="`pwd`"
	cd "$1"
	find -type f \( -not -name "sha256.txt" \) -print0 | sort -z | xargs -r0 sha256sum -b | tee sha256.txt
	cd "$start"
fi
