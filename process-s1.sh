name="Interviews With Monster Girls"
dest="/mnt/Media/ANIME/TV/$(echo "${name}" | sed 's/[\.:?"\/\]/\_/g')"
# sim="--simulate"
# ovr="--overwrite"
if [ ! -d "${dest}" ]; then
	mkdir "$dest"
fi
echo "PROCESSING: Season 1 [${name}]"
./anime-processing-room -d "${dest}" -n "${name}" ${sim} ${ovr} --season 1 --epOff 0 --epMin 1 --epMax 12 --epType 1 --epTypePrefix "#"