# DEPRECATED - USE [THIS](https://gitlab.com/damysteryman/anime-processing-room-net-core) INSTEAD

# Anime Processing Room

Bash script to automate processing of freshly ripped raw .mkv episode files ripped from DVD/Blu-ray into something more understandable, by doing things like:

* Batch name all episode files with correct titles
* Batch set mkv video/audio/subtitle tracks with meaningful names + set which tracks are played by default
* Truncate ("slice") off bloated extra parts at the beginning and end of each file (a lot of rips include a second of two of blackscreen at the beginning at end of each file)
* Mix Video/Audio/Subtitle tracks from multiple sources to create a single .mkv (Example: mix Video track from Japanese Video source with English Dub Audio source, or Fansub Subtitle source)

Lots of auxiliary txt+csv files need to be crafted by the user to provide all the data necessary to do all of this.
Initial release, documentation not yet available. Script not ready for public yet, only uploaded here so that it has a git repo to begin with.